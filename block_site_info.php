<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block Site Information
 *
 * @package    block_site_info
 * @copyright  2017 Fernando Acedo (3-bits.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/user/lib.php');
use block_site_info\fetcher;

class block_site_info extends block_base {
    public function init() {
        $this->title = get_string('pluginname', 'block_site_info');
    }

    public function has_config() {
        return false;
    }

    public function hide_header() {
        return false;
    }

    public function get_content() {
        global $DB;

        if ($this->content !== null) {
            return $this->content;
        }

        // Number of users.
        $usercount = get_users(false);

        // Number of online users.
        $timetoshowusers = 300; // Users on line in the last 5 minutes (in seconds).
        $now = time();
        $isseparategroups = ($this->page->course->groupmode == SEPARATEGROUPS
                             && $this->page->course->groupmodeforce
                             && !has_capability('moodle/site:accessallgroups', $this->page->context));
        $currentgroup = $isseparategroups ? groups_get_course_group($this->page->course) : null;
        $sitelevel = $this->page->course->id == SITEID || $this->page->context->contextlevel < CONTEXT_COURSE;
        $onlineusers = new fetcher($currentgroup, $now, $timetoshowusers, $this->page->context, $sitelevel, $this->page->course->id);
        $minutes  = floor($timetoshowusers / 60);
        $useronlinecount = $onlineusers->count_users();

        // Number of courses.
        $coursetotal = count(get_courses()) - 1;

        // Number of active courses.
        $courseactive = $DB->count_records('course', array('visible' => 1));
        $courseactive = $courseactive - 1;

        // Show data ********************************************************************.
        $this->content = new stdClass;
        $this->content->text  = '<div class="row-fluid">';

        // Users.
        $this->content->text .= '<div class="span3">';
        $this->content->text .= '<div class="site-info-container">';
        $this->content->text .= '<span class="site-info-value">'.$usercount.'</span>';
        $this->content->text .= '<span class="site-info-label">'.get_string('users', 'block_site_info').'</span>';
        $this->content->text .= '</div>';
        $this->content->text .= '</div>';

        // Users online.
        $this->content->text .= '<div class="span3">';
        $this->content->text .= '<div class="site-info-container">';
        $this->content->text .= '<span class="site-info-value">'.$useronlinecount.'</span>';
        $this->content->text .= '<span class="site-info-label">'.get_string('usersonline', 'block_site_info').'</span>';
        $this->content->text .= '</div>';
        $this->content->text .= '</div>';

        // Courses total.
        $this->content->text .= '<div class="span3">';
        $this->content->text .= '<div class="site-info-container">';
        $this->content->text .= '<span class="site-info-value">'.$coursetotal.'</span>';
        $this->content->text .= '<span class="site-info-label">'.get_string('courses', 'block_site_info').'</span>';
        $this->content->text .= '</div>';
        $this->content->text .= '</div>';

        // Active Courses.
        $this->content->text .= '<div class="span3">';
        $this->content->text .= '<div class="site-info-container">';
        $this->content->text .= '<span class="site-info-value">'.$courseactive.'</span>';
        $this->content->text .= '<span class="site-info-label">'.get_string('activecourses', 'block_site_info').'</span>';
        $this->content->text .= '</div>';
        $this->content->text .= '</div>';

        // End show data ****************************************************************.
        $this->content->text .= '</div>';

        return $this->content;
    }
}
